package com.project.web;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.dao.IphoneRemote;
import com.project.dao.IuserRemote;
import com.project.modal.Smartphone;
import com.project.modal.User;




/**
 * Servlet implementation class ControllerServeltSmartphone
 */
@WebServlet("/ControllerServeltSmartphone")
public class ControllerServeltSmartphone extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private static String redirect="smartphone.jsp";

		@EJB
		private IphoneRemote service;
		
		@EJB
		private IuserRemote serviceUser;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServeltSmartphone() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<User> users = serviceUser.findAll();
		request.setAttribute("users", users);
		List<Smartphone> smartphones = service.findAll();
		request.setAttribute("smartphones", smartphones);
		request.getRequestDispatcher(redirect).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		// TODO Auto-generated method stub
		if (action.equalsIgnoreCase("add")) {
			String imei = request.getParameter("imei");
			String user = request.getParameter("user");
			
			service.create(new Smartphone(imei,new User(user)));
			
		}
		
		else if(action.equalsIgnoreCase("delete")) {
			System.out.println("hello");

			String userid = request.getParameter("id");
			Long id=(long) Integer.parseInt(userid);
			service.delteById(new Smartphone(id));
			
			List<Smartphone> smartphones = service.findAll();
			request.setAttribute("smartphones", smartphones);
		}
		else if(action.equalsIgnoreCase("editSmart")) {
			System.out.println("hello");

			String userid = request.getParameter("id");
			Long id=(long) Integer.parseInt(userid);
			Smartphone smartphone=service.findById(new Smartphone(id));
			request.setAttribute("smartphone", smartphone);
        	//redirect = "/editUser.jsp";            

			/*List<User> users = service.findAll();
			request.setAttribute("users", users);*/
		}
		else if(action.equalsIgnoreCase("update")) {
			String userid = request.getParameter("id");
			int id=Integer.parseInt(userid);
			String imei = request.getParameter("imei");
			String user = request.getParameter("userNom");
			
			service.update(new Smartphone(imei,new User(user)));
		}
		doGet(request, response);
	}

}
