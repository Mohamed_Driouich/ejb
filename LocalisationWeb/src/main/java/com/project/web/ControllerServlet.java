package com.project.web;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project.dao.IuserRemote;
import com.project.modal.User;



/**
 * Servlet implementation class ControllerServlet
 */
@WebServlet("/UserController")
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private static String redirect="userJsp.jsp";
	 
	 @EJB
	 private IuserRemote service;
	 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<User> user = service.findAll();
		request.setAttribute("user", user);
		request.getRequestDispatcher(redirect).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getServletPath();
		// TODO Auto-generated method stub
		if (action.equalsIgnoreCase("add")) {
			String Nom = request.getParameter("nom");
			String Prenom = request.getParameter("prenom");
			String Tele = request.getParameter("telephone");
			String email = request.getParameter("email");
			String dateNaiss = request.getParameter("dateNaiss").replace("-", "/");
			service.create(new User(Nom,Prenom,Tele,email, new Date(dateNaiss)));
			System.out.println("s==============================================");
			System.out.println("Nom==============================================");

		}
		else if(action.equalsIgnoreCase("delete")) {
			System.out.println("hello");

			//String userid = request.getParameter("id");
			long id=Long.parseLong(request.getParameter("id"));
			service.delteById(new User(id));
			
			List<User> users = service.findAll();
			request.setAttribute("users", users);
		}
		else if(action.equalsIgnoreCase("editUser")) {
			System.out.println("hello");

			String userid = request.getParameter("id");
			long id=Integer.parseInt(userid);
			User user=service.findById(new User(id));
			request.setAttribute("user", user);
        	//redirect = "/editUser.jsp";            

			/*List<User> users = service.findAll();
			request.setAttribute("users", users);*/
		}
		else if(action.equalsIgnoreCase("update")) {
			String userid = request.getParameter("id");
			int id=Integer.parseInt(userid);
			String Nom = request.getParameter("nom");
			String Prenom = request.getParameter("prenom");
			String Tele = request.getParameter("tele");
			String email = request.getParameter("email");
			String dateNaiss = request.getParameter("dateNaiss").replace("-", "/");
			service.update(new User(Nom,Prenom,Tele,email, new Date(dateNaiss)));
		}
		doGet(request, response);
	}

}
