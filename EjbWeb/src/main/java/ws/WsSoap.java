package ws;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebService;

import com.project.dao.IuserRemote;
import com.project.modal.User;

@WebService
public class WsSoap {
@EJB
private IuserRemote us;

public void create(User t) {
	us.create(t);
}

public void delteById(User t) {
	us.delteById(t);
}

public User update(User t) {
	return us.update(t);
}

public User findById(User t) {
	return us.findById(t);
}

public List<User> findAll() {
	return us.findAll();
}

} 
